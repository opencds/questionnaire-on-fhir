Example URLs:

file:///Users/phillip/projects/opencds/questionnaire-on-fhir/index.html?fhirServerUrl=https%3A%2F%2Fapi2.hspconsortium.org%2Fpayerextract%2Fopen&patientId=353&questionnaireId=350

file:///Users/phillip/projects/opencds/questionnaire-on-fhir/index.html?fhirServerUrl=https%3A%2F%2Fapi2.hspconsortium.org%2Fpayerextract%2Fopen&patientId=123456-78912378-2389798-abcde&questionnaireId=https%3A%2F%2Fapi2.hspconsortium.org%2Fpayerextract%2Fopen%2FQuestionnaire%2F350

Basically, you can serve this off of the filesystem while under development.

Modify the base URL when deployed elsewhere.

Three parameters are required:

fhirServerUrl - a URL-encoded URL to a FHIR server
patientId - the patient id that will be used to obtain patient data from ${fhirServerUrl}
questionnaireId - the questionnaireId that will be used to obtain the questionnaire from ${fhirServerUrl}

questionaireId may also be a URL-encoded URL to another location to the questionnaire.

Interaction with EHR
--------------------

Three functions must be defined and loaded in order to interact with the EHR:

  - onLoad();
    - index.html will call this function
  - openWindow(url);
    - script.js will use this function to attach a jquery listener to all links in the page.
      This is necessary when the browser window is loaded into an iframe or otherwise and is
      required to interact with the parent window to call EHR-defined functions (via, e.g., 
      JavaScript messaging).
  - placeOrder(order);
    - script.js will use this function to attach a listener to the Place Order button.  This
      will allow interaction with, e.g., the parent page (via, e.g., JavaScript messaging).

The existing code base expects an epic.js file (since we are Epic customers), but should be substituted
for a file with the above functions (and any helper code) that interoperates with the EHR.
// thanks StackOverflow!  (http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript)
var getParameterByName = function(name, url) {
  if (!url) {
    url = window.location.href;
  }
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
};

var buildQuestionnaire = function(q) {
  var name = q.title
  $("#q_title").text(name);

  q.item.forEach(function(qi) {

    var visible = "";
    if (qi.enableWhen != undefined) {
      qi.enableWhen.forEach(function(enableWhen) {
        visible = " style=\"display: none;\" ";
        if (enableWhen.answerBoolean != undefined) {
          // console.log(enableWhen);
          $("li#question_" + enableWhen.question).click(function() {
            if ($('input#qr_' + enableWhen.question + ":checked").val() != undefined) {
              if (JSON.parse($('input#qr_' + enableWhen.question + ":checked").val()) == enableWhen.answerBoolean) {
                $("li#question_" + qi.linkId).css('display', 'list-item');
              } else {
                $("li#question_" + qi.linkId).css('display', 'none');
              }
            }
          });
        }
      });
    }
    var rowHtml = "<li id=\"question_" + qi.linkId + "\" " + visible + ">";
    rowHtml += qi.text;
    if (qi.type == "boolean") {
      rowHtml += "<br><input type=\"radio\" name=\"question_" + qi.linkId + "\" id=\"qr_" + qi.linkId + "\" value=\"true\">Yes</input>";
      rowHtml += "<br><input type=\"radio\" name=\"question_" + qi.linkId + "\" id=\"qr_" + qi.linkId + "\" value=\"false\">No</input>";
    } else if (qi.type == "decimal") {
      rowHtml += "<br><input type=\"text\" name=\"question_" + qi.linkId + "\" id=\"qt_" + qi.linkId + "\"></input>";
    }
    rowHtml += "</li>";
    var row = $(rowHtml);
    $("#q_list").append(row);
  });
  $('a').click(function(){ openWindow(this.href); })
};

var questionRegex = /question_(.*)/;

var buildQuestionnaireResponse = function(patient, questionnaireId) {
  var qr = {
    "resourceType": "QuestionnaireResponse",
  };
  qr.questionnaire = {
    "reference": "Questionnaire/" + questionnaireId
  };
  qr.status = "completed";
  qr.subject = {
    "reference": "Patient/" + patient.id
  };
  qr.authored = new Date();
  // start with radio buttons
  var linkId;
  var item;
  var inputs = $(':input[name^=question]');
  if (inputs.size() > 0) {
    qr.item = [];
  }
  inputs.each(function(e) {
    var visible = $(this.parentElement).css('display');
    if (visible == 'list-item') {
      if (this.type == "radio") {
        if (this.checked == true) {
          // console.log(e + ": " + " id: " + this.name + " value: " + this.value);
          linkId = questionRegex.exec(this.name)[1]
          item = {
            "linkId": linkId,
            "answer": [{
              "valueBoolean": this.value
            }]
          };
          qr.item.push(item);
        }
      } else if (this.type == "text") {
        // console.log(e + ": " + " id: " + this.name + " value: " + this.value);
        linkId = questionRegex.exec(this.name)[1];
        item = {
          "linkId": linkId,
          "answer": [{
            "valueDecimal": parseFloat(this.value)
          }]
        };
        qr.item.push(item);
      }
    }
  });
  return qr;
};


var buildParameters = function(patient, observations, questionnaire, questionnaireResponse) {
  var bundle = {
    resourceType: "Bundle",
    type: "document",
    total: 0,
    entry: []
  };

  observations.forEach(function(obs) {
    bundle.entry.push({
      resource: obs
    });
  });
  bundle.entry.push({
    resource: questionnaireResponse
  });
  bundle.entry.push({
    resource: questionnaire
  });
  
  bundle.total = bundle.entry.length
  
  var params = {
    resourceType: "Parameters",
    parameter: [{
      name: "patient",
      resource: patient
    }, {
      name: "inputData",
      resource: bundle
    }]
  };
  return params;
};


var buildGuidanceHTML = function(gr) {
  var id = gr.outputParameters.reference.replace("#","");
  var params = getParams(gr, id);
  $('span#guidance-response').append(getMessageHTML(params));
  // find orderable(s)
  var orderables = findOrderables(params);
  $('span#guidance-response').append("<br><br>");
  orderables.forEach(function(o) {
    $('span#guidance-response').append("<button type=\"button\" id=\"" + o.id + "\">Place Order</button> Order " + o.text + "<br>");
    $("#" + o.id).click(function() {
      placeOrder(o.id);
      //$("#" + o.id).attr("disabled","disabled");
      //$("#" + o.id).text("Order Placed");
    });
  });
};

var getParams = function(gr, id) {
  var params;
  if (gr != undefined && id != undefined) {
    gr.contained.forEach(function(o) {
      if (o.id == id) {
        params = o.parameter;
      }
    });
  }
  return params;
}

var getMessageHTML = function(params) {
  var message;
  if (params != undefined) {
    params.forEach(function(p) {
      if (p.name == "Message_HTML") {
        message = p.valueString;
      }
    });
  }
  return message;
};

var findOrderables = function(params) {
  var orderables = [];
  if (params != undefined) {
    var adResources = getActivityDefinition(params);
    adResources.forEach(function(adr) {
      if (adr.code.coding[0].system == "http://loinc.org") {
        if (adr.code.coding[0].code == "80619-0") {
          orderables.push({
            "id" : "KMMZIKA01",
            "text" : "Zika IgM Antibody Test"
          });
        } else if (adr.code.coding[0].code == "23968-1") {
          orderables.push({
            "id" : "KMMZIKA03",
            "text" : "Dengue virus IgM Antibody Test"
          });
        } else if (adr.code.coding[0].code == "79190-5") {
          orderables.push({
            "id" : "KMMZIKA02",
            "text" : "Zika RNA NAT (serum, urine, and whole blood) and hold serum"
          });
        }
      }
    });
  }
  return orderables;
};

var getActivityDefinition = function(params) {
  var adResources = [];
  params.forEach(function(p) {
    if (p.name == "ActivityDefinition") {
      adResources.push(p.resource);
    }
  });
  return adResources;
};
